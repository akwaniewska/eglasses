package com.example.ala.glassescameraviewer;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.ala.eventaggregator.Event;
import com.example.ala.eventaggregator.EventAggregator;
import com.example.ala.eventaggregator.EventListener;
import com.example.ala.regiondetector.RegionDetector;
import com.example.ala.utils.FrameUtil;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements EventListener, CameraBridgeViewBase.CvCameraViewListener{

    private FaceView mFaceView;
    private TextView textView;
    private CameraBridgeViewBase mOpenCvCameraView;
    private RegionDetector regionDetector;

    private EventAggregator eventAggregator;
    private CascadeClassifier faceCascadeClassifier;
    private CascadeClassifier eyeCascadeClassifier;

    private double heightProportion = 0.6;
    private double widthProportion = 0.3;
    private String tooFarMsg = "Move closer";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mFaceView = (FaceView) findViewById( R.id.face_overlay );
        textView = (TextView) findViewById(R.id.textView);
        textView.setText(tooFarMsg);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        eventAggregator = new EventAggregator();
        eventAggregator.addEventListener(Event.PROCESSED_IMAGE, this);
        eventAggregator.addEventListener(Event.NEW_FACE_DETECTED, this);

        regionDetector = new RegionDetector(eventAggregator);

    }

    public BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    loadCascades();
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setAlpha(0);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    private void loadCascades() {
        faceCascadeClassifier = initializeOpenCVDependencies(R.raw.lbpcascade_frontalface, "lbpcascade_frontalface.xml");
        eyeCascadeClassifier = initializeOpenCVDependencies(R.raw.haarcascade_mcs_eyepair_big, "haarcascade_mcs_eyepair_big.xml");
        eventAggregator.triggerEvent(Event.VISIBLE_CASCADES, faceCascadeClassifier, eyeCascadeClassifier);
    }

    private CascadeClassifier initializeOpenCVDependencies(int i, String name) {
        CascadeClassifier cascadeClassifier = null;
        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = getResources().openRawResource(i);
            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, name);
            FileOutputStream os = new FileOutputStream(mCascadeFile);

            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();

            // Load the cascade classifier
            cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            cascadeClassifier.load(mCascadeFile.getAbsolutePath());
        } catch (Exception e) {
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }
        return cascadeClassifier;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    @Override
    public void onEventOccurred(Event event, final Object parameter, Object parameter2) {
       if(event==Event.PROCESSED_IMAGE){
           final Bitmap bitmap = FrameUtil.matToBitmap((Mat)parameter);
           runOnUiThread(new Runnable() {
               @Override
               public void run() {
                   mFaceView.setBitmap(bitmap);
                   mFaceView.invalidate();
               }
           });
        }
        else if(event==Event.NEW_FACE_DETECTED){
           setDistanceMsg((Rect) parameter);
       }

    }

    private void setDistanceMsg(Rect parameter) {
        final Rect faceRect = parameter;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(faceRect.size().height/(double)mFaceView.getHeight() < heightProportion &&
                        faceRect.size().width/(double)mFaceView.getWidth() < widthProportion) {
                    textView.setText(tooFarMsg);
                    mFaceView.setColor(Color.RED);
                }
                else {
                    textView.setText("");
                    mFaceView.setColor(Color.BLUE);
                }
            }
        });
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(Mat inputFrame) {
        eventAggregator.triggerEvent(Event.NEW_IMAGE, inputFrame, null);
        return inputFrame;
    }

}

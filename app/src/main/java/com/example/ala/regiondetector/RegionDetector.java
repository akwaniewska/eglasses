package com.example.ala.regiondetector;

import com.example.ala.eventaggregator.Event;
import com.example.ala.eventaggregator.EventAggregator;
import com.example.ala.eventaggregator.EventListener;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 * Created by ala on 14/07/16.
 */
public class RegionDetector implements EventListener{
    private final EventAggregator eventAggregator;
    private CascadeClassifier faceCascade;
    private CascadeClassifier eyeCascade;

    public RegionDetector(EventAggregator eventAggregator){
        this.eventAggregator = eventAggregator;
        eventAggregator.addEventListener(Event.NEW_IMAGE, this);
        eventAggregator.addEventListener(Event.VISIBLE_CASCADES, this);
    }

    @Override
    public void onEventOccurred(Event event, Object parameter, Object parameter2) {

        if(event ==Event.VISIBLE_CASCADES) {
            faceCascade = (CascadeClassifier)parameter;
            eyeCascade = (CascadeClassifier)parameter2;
        }
        else if (event==Event.NEW_IMAGE){
            Mat mat = detectRegion((Mat)parameter, faceCascade, "face");
            mat = detectRegion(mat, eyeCascade, "eyes");
            eventAggregator.triggerEvent(Event.PROCESSED_IMAGE, mat, null);
        }
    }

    private Mat detectRegion(Mat mat, CascadeClassifier cascade, String region) {
        Rect rect;
        if (!mat.empty()) {
            Mat grayscale = mat.clone();
            Imgproc.cvtColor(mat, grayscale, Imgproc.COLOR_RGBA2RGB);

            MatOfRect regions = new MatOfRect();

            // Use the classifier to detect regions
            if (cascade != null) {
                cascade.detectMultiScale(grayscale, regions);
            }


            // If there are any regions found, draw a rectangle around it
            Rect[] objectArray = regions.toArray();
            if (objectArray.length>0) {
                drawRect(mat, region, objectArray);
            }
        }
        return mat;
    }

    private void drawRect(Mat mat, String region, Rect[] objectArray) {
        Rect rect;
        rect = new Rect(objectArray[0].tl(), objectArray[0].br());
        if (region == "face") {
            eventAggregator.triggerEvent(Event.NEW_FACE_DETECTED, rect, null);
        }
        else if (region == "eyes") {
            rect = new Rect(rect.x, rect.y-2*rect.height, rect.width, rect.height);
        }
        Imgproc.rectangle(mat, rect.tl(), rect.br(), new Scalar(0, 255, 0, 255), 3);
    }
}
